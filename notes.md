Uruchamianie:
npm install
npm start

Test:
npm test

Build:
npm run build

Założenia:
- jako bundler użyto webpacka, zainstalowano wszystkie niezbędne loadery
- babel zapewnia transpilacje kodu ES6 na ES5 oraz polyfill dla przeglądarek stanowiących >0.25% rynku
- postcss zapewnia min. autoprefixing
- do stylizacji wykorzystano SASS'a wraz z metodoliga BEM
- do testow wykorzystano Mocha oraz Chai
- sortowanie elementów wykonywane jest za pomocą metody sort klasy Array, metoda ta działa poprawnie jeżeli wewnętrzna metoda compare zwraca wartosci zgodne z dokumentacja metody sort, tj. przy porownywaniu a i b:
    a) zwracane jest 0, gdy a == b
    b) zwracana jest wartość dodatnia, gdy a > b
    C) zwracana jest wartość ujemna, gdy a < b
 w związku z tym testując metode sortItemsList sprawdzono jedynie metode compare
- dodatkowo przetestowano metode inputNumberValidator, która ma zapewnic, że użytkownik nie moze wpisać litery w input